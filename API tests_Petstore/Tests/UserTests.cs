﻿using API_tests_Petstore.Infrastructure;
using API_tests_Petstore.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System;
using System.Net;

namespace API_tests_Petstore
{
    [TestClass]
    public class UserTests
    {       
        [TestMethod]
        public void AddUser()
        {        

            var expected = HttpStatusCode.OK;

            User user = new User()
            {
                Id = 1,
                Username = "user1",
                FirstName = "ann",
                LastName = "brusentsova",
                Email = "anniebrusentsova@gmail.com",
                Password = "qwerty1234",
                Phone = "+375291232323",
                UserStatus = 0

            };
          
            RestClient client = RestClientFactory.GetRestClient();         


            var request = new RestRequest("/user", Method.Post);
            request.AddJsonBody(user);

            try
            {
                var response = client.Execute(request);
                var actual = response.StatusCode;
              
               
                Assert.AreEqual(expected, actual);
            }
            catch (Exception ex)
            {             
                
                Assert.Fail("Exception thrown: " + ex.Message);
            }
            finally
            {
                client = null;
            }

        }

        [TestMethod]
        public void GetUserByUsername()
        {          
            var expected = HttpStatusCode.OK;

            RestClient client = RestClientFactory.GetRestClient();


            var request = new RestRequest("/user/{username}", Method.Get);
            request.AddUrlSegment("username", "user1");

            try
            {
                var response = client.Execute(request);
                var actual = response.StatusCode;

               
                Assert.AreEqual(expected, actual);
            }

            catch (Exception ex)
            {              
                Assert.Fail("Exception thrown: " + ex.Message);
            }
            finally
            {
                client = null;
            }
        }

        [TestMethod]
        public void UpdateUser()
        {           

            var expected = HttpStatusCode.OK;

            User user = new User()
            {
                Id = 1,
                Username = "user1",
                FirstName = "ann",
                LastName = "brusentsova",
                Email = "anniebrusentsova@gmail.com",
                Password = "qwerty1234",
                Phone = "+375291232323",
                UserStatus = 0

            };
          
            RestClient client = RestClientFactory.GetRestClient();

            var request = new RestRequest("/user/{username}", Method.Put);
            request.AddUrlSegment("username", "user1");

            request.AddJsonBody(user);
          
            try
            {
                var response = client.Execute(request);
                var actual = response.StatusCode;

                Assert.AreEqual(expected, actual);
            }
            catch (Exception ex)
            {
                Assert.Fail("Exception thrown: " + ex.Message);
            }

            finally
            {
                client = null;
            }

        }

        [TestMethod]
        public void DeleteUser()
        {
            var expected = HttpStatusCode.OK;

            RestClient client = RestClientFactory.GetRestClient();


            var request = new RestRequest("/user/{username}", Method.Delete);
            request.AddUrlSegment("username", "user1");

            try
            {
                var response = client.Execute(request);
                var actual = response.StatusCode;
              
                Assert.AreEqual(expected, actual);
            }
            catch (Exception ex)
            {                
                Assert.Fail("Exception thrown: " + ex.Message);
            }

            finally
            {
                client = null;
            }

        }

    }
}
