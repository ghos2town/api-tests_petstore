﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_tests_Petstore.Infrastructure
{
    public class RestClientFactory
    {
        public static RestClient GetRestClient()
        {
            return new RestClient("https://petstore.swagger.io/v2");
        }

    }
}
